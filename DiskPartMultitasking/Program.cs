﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

namespace DiskPartMultitasking
{
    class Program
    {
        static void Main(string[] args)
        {
            int diskStartValue = 1;
            int numberOfDisks = 1;

            if (args.Length > 0)
            {
                String diskStartCommand = "-ds";
                String numDisksCommand = "-nd";
                int diskStartIndex = Array.IndexOf(args, diskStartCommand) + 1;
                int numDisksIndex = Array.IndexOf(args, numDisksCommand) + 1;

                if(diskStartIndex > 0)
                {
                    diskStartValue = Convert.ToInt32(args[diskStartIndex]);
                }

                if(numDisksIndex > 0)
                {
                    numberOfDisks = Convert.ToInt32(args[numDisksIndex]);
                }
            }

            Console.WriteLine("diskStartValue:" + diskStartValue.ToString());
            Console.WriteLine("numberOfDisks:" + numberOfDisks.ToString());
            Console.WriteLine("Continue? y/n");
            string input = Console.ReadLine();
            if(input.ToLower() == "y" || input.ToLower() == "yes")
            {
                var formatters = Enumerable.Range(diskStartValue, numberOfDisks)
                    .Select(diskNumber => new DiskFormat(diskNumber))
                    .ToList();

                foreach (var formatter in formatters)
                    formatter.WaitForExit();
                
                Console.WriteLine("The task is completed.  Behold the irony and press Enter to Exit");
                input = Console.ReadLine();
            }
        }//end Main

        //Utility Class
        public class DiskFormat
        {
            public int PartitionNumber { get; set; }
            
            public DiskFormat(int partitionNumber)
            {
                PartitionNumber = partitionNumber;
            }
            public void Format()
            {
                _process = new Process();
                _process.StartInfo = new ProcessStartInfo
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    FileName = @"C:\Windows\System32\diskpart.exe",
                };

                _process.Exited += (sender, e) =>
                {
                    string output = _process.StandardOutput.ReadToEnd();
                    Console.WriteLine("Disk " + PartitionNumber + " has been completed.  Output: " + output);
                };

                _process.Start();
                _process.StandardInput.WriteLine("select disk " + PartitionNumber.ToString());
                _process.StandardInput.WriteLine("clean");
                _process.StandardInput.WriteLine("create partition primary");
                _process.StandardInput.WriteLine("select partition 1");
                _process.StandardInput.WriteLine("format fs=fat32 quick");
                _process.StandardInput.WriteLine("active");
                _process.StandardInput.WriteLine("assign");
                _process.StandardInput.WriteLine("exit");
            }

            public void WaitForExit()
            {
                _process.WaitForExit();
            }

            Process _process;
        }
    }
}